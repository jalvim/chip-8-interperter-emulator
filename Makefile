# Arquivo para compilação do projeto de emulação 
# do sistema CHIP-8 com bibliotecas e flags necessárias
# para tanto.

# COMPILAR COM: $ make all clean

# Declaração de comiplador utilizado e flags de compilação.
CC = gcc
CFLAGS  = -g -Wall
INFLAGS = -c -Wall

# Declaração de flags de bibliotecas necessárias para a compilação do arquivo.
LIBFLAGS = `sdl2-config --cflags --libs`
CHIP8 = chip8
TARGET = main

# Label de linkagem de objetos.
all: $(TARGET).o $(CHIP8).o
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).o $(CHIP8).o $(LIBFLAGS)

# Label de compilação do objeto primário.
$(TARGET).o: $(CHIP8).o
	$(CC) $(INFLAGS) $(TARGET).c $(LIBFLAGS)

# Label de compilação do objeto secundário.
$(CHIP8).o: $(CHIP8).h
	$(CC) $(INFLAGS) $(CHIP8).c $(LIBFLAGS)

# Label de limpeza.
clean: $(TARGET)
	rm *.o
