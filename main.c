#include <stdlib.h>
#include <stdio.h>
#include "chip8.h"

int get_file_size(FILE *fp) {
	// Função de estimação do tamanho do arquivo.
	int n;
	fseek(fp, 0L, SEEK_END);
	n = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	return n;
}

void load_game(int size, FILE *fp, byte *buff, int off) {
	// Funçao de carregamento do jogo na memória de programa.
	fread(buff + off, 1, size, fp);
	buff[size + off] = EOF;
}

void set_globals() {
	// Faz variávels globais adquirirem seus valores iniciais.
	PC = 0x200;
	render = NULL;
	main_window = NULL;
	stack_ptr = 0;
	I_ptr = 0;
	clear_mem_screen();
}

int main (int argc, char **argv) {
	FILE *fp = fopen(argv[1], "r");
	int  size;
	func_ptr op_func;
	opcode   op;
	set_globals();

	size = get_file_size(fp);
	load_game(size, fp, MEM, PC);
	fclose(fp);

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {		// Verifica a inicialização correta do SDL2.
		print_error("Framework gráfico não iniciado corretamente.");
		return -1;
	}

	init_main_window(&main_window, &render);
	prep_render(render);

	// VERIFICAR VIABILIDADE DA CONDIÇÃO DE PARADA.
	while (1) {
		// VERIFICAR COMO USAR OS DELAYS DE DISPLAY E SOM
		//SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
		op = fetch_opcode();
		DECMACRO(op, op_func);
		op_func(op);

		if (delay_timer > 0) 
			delay_timer --;

		if (sound_timer > 0) {
			if (sound_timer == 1) 
				printf("BEEP\n");

			sound_timer --;
		}

		SDL_PollEvent(&event);
		if (event.type == SDL_QUIT) 
			break;
	}

	SDL_DestroyWindow(main_window);
	SDL_DestroyRenderer(render);
	SDL_Quit();
	// free(MEM);
	// free(op_func);

	return 0;
}
