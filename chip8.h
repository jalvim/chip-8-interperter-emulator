#ifndef __CHIP_8__
// DEFINE MACRO DE VERIFICAÇÃO DE INCLUSÃO DA BIBLIOTECA.
#define __CHIP_8__		1

// INCLUSÃO DA BIBLIOTECA SDL P/ CRIAÇÃO DE JANELA PRINCIPAL.
#include <SDL2/SDL.h>

// MACROS DE TAMANHO DA JANELA CRIADA
#define STEP			4
#define HEIGHT 			STEP*32
#define WIDTH			STEP*64

// MACROS DE ESTADOS LÓGICOS.
#define TRUE			1
#define FALSE			0

// MACROS DE COMPRIMENTO DE PALAVRAS
#define NIB			4
#define BYT			8
#define WOR			16

// MACRO DE DEFINIÇÃO DE CORES ANSI.
#define ANSI_COLOR_MAGENTA 	"\x1b[35m" 
#define ANSI_COLOR_RESET 	"\x1b[0m"
#define ANSI_COLOR_RED 		"\x1b[31m"

// MACRO PARA CICLO DE DECODIFICAÇÃO DE INSTRUÇÃO.
#define DECMACRO(op, ptr) 	do {				\
	switch (op & 0xF000) {					\
		case 0x0 << 12:					\
			if ((op & 0xFF) == 0XE0)		\
				ptr = CLS;			\
			else if ((op & 0xFF) == 0xEE)		\
				ptr = RET;			\
			else { 					\
				ptr = NOP;			\
				goto DEF;			\
			}					\
			break;					\
		case 0x1 << 12:					\
			ptr = JP;				\
			break;					\
		case 0x2 << 12:					\
			ptr = CALL;				\
			break;					\
		case 0x3 << 12:					\
			ptr = SEX;				\
			break;					\
		case 0x4 << 12:					\
			ptr = SNEX;				\
			break;					\
		case 0x5 << 12:					\
			ptr = SEXY;				\
			break;					\
		case 0x6 << 12:					\
			ptr = MOVX;				\
			break;					\
		case 0x7 << 12:					\
			ptr = ADDX;				\
			break;					\
		case 0x8 << 12:					\
			switch (op & 0x0F) {			\
				case 0x0:			\
					ptr = MOVY;		\
					break;			\
				case 0x1:			\
					ptr = ORXY;		\
					break;			\
				case 0x2:			\
					ptr = ANDXY;		\
					break;			\
				case 0x3:			\
					ptr = XORXY;		\
					break;			\
				case 0x4:			\
					ptr = ADDXY;		\
					break;			\
				case 0x5:			\
					ptr = SUBXY;		\
					break;			\
				case 0x6:			\
					ptr = SHRX;		\
					break;			\
				case 0x7:			\
					ptr = SUBYX;		\
					break;			\
				case 0xE:			\
					ptr = SHLX;		\
					break;			\
				default:			\
					ptr = NOP;		\
					goto DEF;		\
			}					\
			break;					\
		case 0x9 << 12:					\
			ptr = SNEXY;				\
			break;					\
		case 0xA << 12:					\
			ptr = MVI;				\
			break;					\
		case 0xB << 12:					\
			ptr = JMI;				\
			break;					\
		case 0xC << 12:					\
			ptr = RND;				\
			break;					\
		case 0xD << 12:					\
			ptr = SPRITE;				\
			break;					\
		case 0xE << 12:					\
			if ((op & 0xFF) == 0x9E) 		\
				ptr = SKPR;			\
			else if ((op & 0xFF) == 0xA1) 		\
				ptr = SKUP;			\
			else {					\
				ptr = NOP;			\
				goto DEF;			\
			}					\
			break;					\
		case 0xF << 12:					\
			switch(op & 0xFF) {			\
				case 0x07:			\
					ptr = GDELAY;		\
					break;			\
				case 0x0A:			\
					ptr = KEYVR;		\
					break;			\
				case 0x15:			\
					ptr = SDELAY;		\
					break;			\
				case 0x18:			\
					ptr = SSOUND;		\
					break;			\
				case 0x1E:			\
					ptr = ADI;		\
					break;			\
				case 0x29:			\
					ptr = FONT;		\
					break;			\
				case 0x30:			\
					ptr = XFONT;		\
					break;			\
				case 0x33:			\
					ptr = BCD;		\
					break;			\
				case 0x55:			\
					ptr = STR;		\
					break;			\
				case 0x65:			\
					ptr = LDR;		\
					break;			\
				default:			\
					ptr = NOP;		\
					goto DEF;		\
			}					\
			break;					\
DEF:								\
		default:					\
			print_error("Opcode desconhecido");	\
			fprintf(stderr, "0x%x\n", op); 		\
			ptr = NOP;				\
	}							\
} while (0); 

// DEFINIÇÃO DE TIPOS CLÁSSICOS.
typedef unsigned char byte;				// Definição de unidade de byte.
typedef unsigned short opcode;				// Definição de tipo de opcode.
typedef void (*func_ptr) (opcode);			// Definição de tipo de ponteiro de função.

// DEFINIÇÃO DE ESTRUTURA P/ REGISTRADORES.
typedef struct Register{				// Definição de estrutura p/ registradores.
	byte status;     				// Definição de estrutura p/ registradores.
} Register;             				// Definição de estrutura p/ registradores.

// DECLARAÇÃO DE PARÂMETROS GLOBAIS EM "HARDWARE" DA MÁQUINA VIRTUAL.
unsigned int stack_ptr;					// Definição do ponteiro de pilha.
unsigned int I_ptr;					// Definição do ponteiro de pilha.
unsigned int stack[16];					// Definição da pilha como vetor.
Register     Vx[16];					// Definição dos registradores utilizados no sistema.

// DECLARAÇÃO DE GRANDEZAS GLOBAIS DA INTERFACE GRÁFICA.
SDL_Window   *main_window;				// Criação da janela principal.
SDL_Renderer *render;					// Declara renderizador para linhas aleatórias.
SDL_Event    event;					// Declaração de pilha de eventos globais. Loop é definido em main.

// DECLARAÇÃO DE ABSTRAÇÕES DA MÁQUINA VIRTUAL, COMO CÓDIGO DE OPERAÇÃO.
unsigned int PC;					// Inicializa PC como 0x200, endereço usual.
byte MEM[4096];						// Declara buffer de memória de programa.
byte delay_timer, sound_timer;				// Cria variáveis globais de timers.

// DECLARAÇÃO DE MEMÓRIA DE PREENCHIMENTO DE TELA.
unsigned long mem_screen[32];			// Modo de tela tamanho 64x32. --> Acesso bit a bit.

// DECLARAÇÕES DE FUNÇÕES DE INSTRUÇÕES.
void NOP(opcode);
void CLS(opcode);
void RET(opcode);
void JP(opcode);
void CALL(opcode);
void SEX(opcode);
void SNEX(opcode);
void SEXY(opcode);
void MOVX(opcode);
void ADDX(opcode);
void MOVY(opcode);
void ORXY(opcode);
void ANDXY(opcode);
void XORXY(opcode);
void ADDXY(opcode);
void SUBXY(opcode);
void SHRX(opcode);
void SUBYX(opcode);
void SHLX(opcode);
void SNEXY(opcode);
void MVI(opcode);
void JMI(opcode);
void RND(opcode);
void SPRITE(opcode);
void SKPR(opcode);
void SKUP(opcode);
void GDELAY(opcode);
void KEYVR(opcode);
void SDELAY(opcode);
void SSOUND(opcode);
void ADI(opcode);
void FONT(opcode);
void XFONT(opcode);
void BCD(opcode);
void STR(opcode);
void LDR(opcode);

// DECLARAÇÃO DE FUNÇÕES RELATIVAS À INTERFACE GRÁFICA.
void init_main_window(SDL_Window **, SDL_Renderer **);

// DECLARAÇÃO DE FUNÇÕES DE CICLO COMPUTACIONAL.
opcode fetch_opcode(void);

// DECLARAÇÃO DE FUNÇÕES DE DEBUG.
void print_error(char *);

// DECLARAÇÃO DE FUNÇÕES GRÁFICAS POR EXCELÊNCIA
void prep_render(SDL_Renderer *);
void draw_old_screen(byte, byte);
void clear_mem_screen(void);
void detect_colision(byte, byte);
byte ascii_remap(byte);
#endif
