// INCLUSÃO DA BIBLIOTECA DESENVOLVIDA P/ O EMULADOR.
#include "chip8.h"
#include "sprite.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// VERIFICAÇÃO DA IMPORTAÇÃO CORRETA DA BIBLIOTECA FEITA.
#ifndef __CHIP_8__
#include <SDL2/SDL.h>
#define STEP 			4
#define HEIGHT 			STEP*32
#define WIDTH			STEP*64
#endif

void print_error(char *erro) {
	// Função de impressão de erro de execução de operação.
	fprintf(stderr, 
		ANSI_COLOR_RED
		"ERRO: "
		ANSI_COLOR_RESET
		"Erro na operação: %s\n",
		erro
	);
}

opcode fetch_opcode() {
	// Prepara próxima operação (todas tem 2 bytes).
	opcode op;
	op = (MEM[PC] << 8) | (MEM[PC + 1] & 0xFF);

	return op;
}

// CICLO DE FUNÇÕES DE MANEJO DO DISPLAY.
void init_main_window(SDL_Window **window, SDL_Renderer **render_r) {
	// Função e inicialização da janela principal
	srand(time(NULL));				// Inicialização da semente de aleatoriedade.
	*window = SDL_CreateWindow(
		"Main Window",				// Cria janela SDL2.
		SDL_WINDOWPOS_UNDEFINED,         	// Cria janela SDL2.
		SDL_WINDOWPOS_UNDEFINED,         	// Cria janela SDL2.
		WIDTH,                           	// Cria janela SDL2.
		HEIGHT,                          	// Cria janela SDL2.
		SDL_WINDOW_SHOWN  			// Flag de janela exposta.
	);

	if (*window == NULL) 				// Verificação adequada da janela.
		print_error("Criação de janela.");	// Imprime erro na saída de erro padrão.

	*render_r = SDL_CreateRenderer(   		// Inicializa o renderizador.
		*window,                  		// Inicializa o renderizador.
		-1,                       		// Inicializa o renderizador.
		SDL_RENDERER_ACCELERATED |		// Inicializa o renderizador. 
		SDL_RENDERER_PRESENTVSYNC 		// Inicializa o renderizador.
	);
}

void prep_render(SDL_Renderer *render_r) {
	// Função de reinicialização de renderizador de textura.
	render_r = SDL_CreateRenderer(
		main_window, 				// Cria objeto render p/ manipulação da tela.
		-1, 
		SDL_RENDERER_ACCELERATED
	);
	SDL_RenderClear(render_r);
	SDL_SetRenderDrawColor(
		render_r, 
		0, 0, 0, 
		SDL_ALPHA_OPAQUE
	);
	SDL_RenderPresent(render_r);
}

void draw_old_screen(byte x, byte y) {
	// Função de armazenamento de estado de tela na memória.
	unsigned long bit = 1 << (63 - x);		// Declara bit na posição habilitada do display.
	mem_screen[y] ^= bit;				// Inverte o estado do bit em questão, atualizando a tela.
}

void clear_mem_screen() {
	// Função de reinicialização dos estados de tela.
	for (int i = 0; i < 32; i++) {			// Itera por cada coluna da memória.
		mem_screen[i] = 0;			// Iguala ela toda a zero.
	}
}

void detect_colision(byte pos_x, byte pos_y) {
	// Função de detecção de colisão.
	unsigned long bit = 1 << (64 - pos_x);		// Declara bit na posição habilitada do display.
	if(mem_screen[pos_y] & bit)			// Verifica se o píxel já está preenchido.
		Vx[0xF].status = 1;			// Se sim, fax VF = 1.
	else						// Caso o contrário, ...
		Vx[0xF].status = 0;			// ... ele é feito 0.
}

byte get_mem_state(byte pos_x, byte pos_y) {
	byte ret;					// Declara var. de retorno.
	ret = (mem_screen[pos_y] & 			// Encontra o píxel em questão.
	      (1 << (63 - pos_x)) ? 1 : 0);		// Encontra o píxel em questão.

	return ret;
}

void draw_point(int pos_x, int pos_y) {
	// Função de desenho de ponto com tamanho artificial de píxel com STEP.
	for (int xx = 0; xx < STEP; xx++)	// Itera por dimensão "x" de sub-píxel.
		for (int yy = 0; yy < STEP; yy++)	// Itera por dimensão "y" de sub-píxel.
			SDL_RenderDrawPoint(		// Desenha ponto na posição (pos_x + x, pos_y + y).
				render,         	// Desenha ponto na posição (pos_x + x, pos_y + y).
				pos_x + xx,		// Desenha ponto na posição (pos_x + x, pos_y + y).
				pos_y + yy		// Desenha ponto na posição (pos_x + x, pos_y + y).
			);
}

byte ascii_remap(byte dig) {
	// Função de remapeamento das chaves usadas.
	char buff[100];
	byte ret;
	switch(dig) {
		case 0x01:				// 1
			ret = 0x31;
			break;
		case 0x02:				// 2
			ret = 0x32;
			break;
		case 0x03:				// 3
			ret = 0x33;
			break;
		case 0x0C:				// C
			ret = 0x34;			// 4
			break;
		case 0x04:				// 4
			ret = 0x71;			// q
			break;
		case 0x05:				// 5
			ret = 0x77;			// w
			break;
		case 0x06:				// 6
			ret = 0x65;			// e
			break;
		case 0x0D:				// D
			ret = 0x72;			// r
			break;
		case 0x07:				// 7
			ret = 0x61;			// a
			break;
		case 0x08:				// 8 
			ret = 0x73;			// s
			break;
		case 0x09:				// 9
			ret = 0x64;			// d
			break;
		case 0x0E:				// E
			ret = 0x66;			// f
			break;
		case 0x0A:				// A
			ret = 0x7A;			// z
			break;
		case 0x00:				// 0
			ret = 0x78;			// x
			break;
		case 0x0B:				// B
			ret = 0x63;			// c
			break;
		case 0x0F:				// F
			ret = 0x76;			// v
			break;
		default:				// Caso de excessão.
			goto error; 
	}

	return ret;					// Retorna código ASCII remapeado.

error:
	sprintf(buff, "Tecla fora do teclado: 0x%x", dig);
	print_error(buff);
}

// CICLO DE IMPLEMENTAÇÃO DAS FUNÇÕES DE INSTRUÇÕES
void NOP(opcode op) {
	// Função de não operação.
	int inc = 2;

	PC += inc;
}

void CLS(opcode op) {
	// Função de limpeza da tela.
	int inc = 2;
	SDL_RenderClear(render);			// Limpa conteúdo presente no renderizador.
	SDL_SetRenderDrawColor(render, 			// Inicia renderização com cor escura.
		0, 0, 0, 				// Coloca trinca RGB como (0, 0, 0).
		SDL_ALPHA_OPAQUE			// Compõe a tela como opaca.
	);
	clear_mem_screen();				// Limpa memória de estado de tela

	if(render == NULL || main_window == NULL) {	// Verifica erro na limpeza da tela.
		print_error("Janela não foi devidamente limpada.");
	}

	PC += inc;
}

void RET(opcode op) {
	// Função de retorno de subrotina.
	//PC = stack_pop(stack_ptr);
	stack_ptr --;
	PC = stack[stack_ptr];
}

void JP(opcode op) {
	// Função de salto incondicional.
	PC = op & 0x0FFF;
}

void CALL(opcode op) {
	// Função de inicilalização de subotina.
	int inc = 2;
	PC += inc;
	stack[stack_ptr] = PC;
	stack_ptr ++;
	PC = op & 0x0FFF;

	if(stack_ptr >= 16) 
		print_error("STACK OVERFLOW");
}

void SEX(opcode op) {
	// Função de setagem do registrador VX em caso de igualdade.
	int inc = 2;					// Cria var. de incremento.
	byte RR =  op & 0x00FF;				// Recupera byte RR da instrução.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice do registrador.
	if(RR == (Vx + X)->status) { 			// Compara RR com estado do reg.
		inc = 4;				// Aumenta incremento p/ pular inst.
	}

	PC += inc;					// Soma incremento ao PC.
}

void SNEX(opcode op) {
	// Função de setagem do registrador VX em caso de desigualdade.
	int inc = 2;					// Cria var. de incremento.
	byte RR =  op & 0x00FF;				// Recupera byte RR da instrução.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice do registrador.
	if(RR != (Vx + X)->status) 			// Compara RR com estado do reg.
		inc = 4;				// Aumenta incremento p/ pular inst.

	PC += inc;					// Soma incremento ao PC.
}

void SEXY(opcode op) {
	// Função de setagem dos registradores VX e VY.
	int inc = 2;					// Cria var. de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte Y  = (op & 0x00F0) >> 4;			// Recupera o índice Y do registrador.
	if((Vx + Y)->status == (Vx + X)->status) 	// Compara registradores.
		inc = 4;				// Aumenta incremento p/ pular inst.

	PC += inc;					// Soma incremento ao PC.
}

void MOVX(opcode op) {
	// Move dados p/ registrador VX.
	int inc = 2;					// Cria var. de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte RR =  op & 0x00FF;				// Recupera byte RR da instrução.
	(Vx + X)->status = RR;				// Carrega valor RR em VX.

	PC += inc;					// Incrementa o PC.
}

void ADDX(opcode op) {
	// Soma conteúdo no registrador VX.
	int inc = 2;					// Cria var. de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte RR =  op & 0x00FF;				// Recupera byte RR da instrução.
	(Vx + X)->status += RR & 0xFF;			// Soma valor RR a VX.

	PC += inc;					// Incrementa o PC.
}

void MOVY(opcode op) {
	// Move dados p/ registrador VY.
	int inc = 2;					// Cria var. de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte Y  = (op & 0x00F0) >> 4;			// Recupera o índice Y do registrador.
	(Vx + X)->status = Vx[Y].status & 0xFF;		// Carrega valor de VY em VX.

	PC += inc;					// Incrementa o PC.
}

void ORXY(opcode op) {
	// Realiza operação OU com regs. VX e VY.
	int inc = 2;					// Cria var. de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte Y  = (op & 0x00F0) >> 4;			// Recupera o índice Y do registrador.
	(Vx + X)->status |= Vx[Y].status & 0xFF;	// Efetua operação lógica de OU com reg VY.

	PC += inc;					// Incrementa o PC.
}

void ANDXY(opcode op) {
	// Realiza operação E com regs. VX e VY.
	int inc = 2;					// Cria var. de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte Y  = (op & 0x00F0) >> 4;			// Recupera o índice Y do registrador.
	(Vx + X)->status &= Vx[Y].status & 0xFF;	// Efetua operação lógica de E com reg VY.

	PC += inc;					// Incrementa o PC.
}

void XORXY(opcode op) {
	// Realiza operação XOU com regs. VX e VY.
	int inc = 2;					// Cria var. de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte Y  = (op & 0x00F0) >> 4;			// Recupera o índice Y do registrador.
	(Vx + X)->status ^= (Vx[Y].status & 0xFF);	// Efetua operação lógica de E com reg VY.

	PC += inc;					// Incrementa o PC.
}

void ADDXY(opcode op) {
	// Soma registradores VX e VY.
	int  sum, inc = 2;				// Declara variáveis de incremento e de soma.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte Y  = (op & 0x00F0) >> 4;			// Recupera o índice Y do registrador.
	sum = Vx[X].status + Vx[Y].status;		// Soma os valores em ambos os registradores.
	if (sum > 255) {				// Verifica se a soma é maior q um byte.
		Vx[X].status = sum & 0xFF;		// Adquire último byte da soma.
		Vx[0xF].status = 1;			// Faz VF = 1, indicando carry.
	} else {					// Caso o contrário, ...
		Vx[X].status = sum;			// ... Faz de VX o valor da soma e ...
		Vx[0xF].status = 0;			// ... Faz VF = 0.
	}

	PC += inc;					// Incrementa o PC.
}

void SUBXY(opcode op) {
	// Subtrai registradores VX e VY.
	int inc = 2;					// Declara variavel de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte Y  = (op & 0x00F0) >> 4;			// Recupera o índice Y do registrador.
	if (Vx[X].status > Vx[Y].status) 
		Vx[0xF].status = 1;
	else
		Vx[0xF].status = 0;

	Vx[X].status -= Vx[Y].status;
	PC += inc;
}

void SHRX(opcode op) {
	// Rotaciona VX p/ direita.
	int inc = 2;					// Declara incremento do PC.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	Vx[0xF].status = Vx[X].status & 1;		// Seta o registrador VF como o último byte de VX.
	Vx[X].status >>= 1;				// Rotaciona o registrador p/ direita.

	PC += inc;					// Incrementa o PC.
}

void SUBYX(opcode op) {
	// Subtrai registradores VY e VX.
	int inc = 2;					// Declara variavel de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte Y  = (op & 0x00F0) >> 4;			// Recupera o índice Y do registrador.
	if (Vx[Y].status > Vx[X].status) 
		Vx[0xF].status = 1;
	else
		Vx[0xF].status = 0;

	Vx[X].status = Vx[Y].status - Vx[X].status;
	PC += inc;
}

void SHLX(opcode op) {
	// Rotaciona VX p/ esquerda.
	int inc = 2;					// Declara variavel de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	Vx[X].status <<= 1;

	PC += inc;
}

void SNEXY(opcode op) {
	// Pula instrução se VX != VY.
	int inc = 2;					// Cria var. de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte Y  = (op & 0x00F0) >> 4;			// Recupera o índice Y do registrador.
	if((Vx + Y)->status == (Vx + X)->status) 	// Compara registradores.
		inc = 4;				// Aumenta incremento p/ pular inst.

	PC += inc;					// Soma incremento ao PC.
}

void MVI(opcode op) {
	// Move informação para I.
	int inc = 2;
	I_ptr   = op & 0x0FFF;
	 
	PC += inc;
}

void JMI(opcode op) {
	// Salta a memória de programa p/ endereço + V0.
	PC = (op & 0x0FFF) + Vx[0].status;
}

void RND(opcode op) {
	// Calcula número aleatório e soma a VX.
	int inc = 2;					// Cria var. de incremento.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte kk =  op & 0x00FF;				// Recupera byte "kk" no código de operação.
	Vx[X].status = kk & (byte) rand();		// Atualiza valor de VX com operação de E entre "kk" e rand.

	PC += inc;					// Incrementa o PC.
}

void SPRITE(opcode op) { // Seleciona e posiciona o SPRITE na tela.
	int inc = 2;
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte Y  = (op & 0x00F0) >> 4;			// Recupera o índice Y do registrador.
	byte n  = (op & 0x000F);			// Número de bytes do sprite (dimensão Y).
	byte pos_x1 = Vx[X].status * STEP;		// Determina posição X.
	byte pos_y1 = Vx[Y].status * STEP;		// Determina posição Y.
	byte pos_x2 = Vx[X].status;			// Determina posição X virtual.
	byte pos_y2 = Vx[Y].status;			// Determina posição Y virtual.
	byte draw_state, state;				// Criação dos estados atuais de pintura.
	Vx[0xF].status = 0;
	for (int y = 0, i = 0; y < n*STEP; y += STEP, i++) {	// Itera sobre a dimensão "y" e sobre ínidce "i"
		for (int x = 0, j = 0; j < 8; x += STEP, j++) {	// Itera sobre a dimensão "x".
			state = MEM[I_ptr + i] & (1 << (7-j)) ? 1 : 0;	// Define estado do píxel.
			draw_state = get_mem_state(			// Readquire estado de píxel na tela.
				pos_x2 + j,        			// Readquire estado de píxel na tela.
				pos_y2 + i         			// Readquire estado de píxel na tela.
			);
			if (state) {
				 draw_old_screen(		// Carrega estado atual na memória de display.
					(pos_x2 + j),		// Carrega estado atual na memória de display. 
					(pos_y2 + i)		// Carrega estado atual na memória de display.
				 );

				if (draw_state)
					Vx[0xF].status = 1;

				if (draw_state ^= 1)
					SDL_SetRenderDrawColor(
					       render, 
					       0xFF, 0xFF, 0xFF, 
					       SDL_ALPHA_OPAQUE 
					);
				else
					SDL_SetRenderDrawColor(
					       render, 
					       0x00, 0x00, 0x00, 
					       SDL_ALPHA_OPAQUE 
					);

				draw_point(         	// Desenha píxel com base em sub-píxeis.
				       (pos_x1 + x),	// Desenha píxel com base em sub-píxeis. 
				       (pos_y1 + y) 	// Desenha píxel com base em sub-píxeis.
				);
			}
		}
	}

	SDL_RenderPresent(render);			// Carrega renderização.
	SDL_Delay(20);					// Faz um Delay de 20 ms.

	PC += inc;					// Incrementa o PC.
}

void SKPR(opcode op) {
	// Pula instrução se alguma tecla está apertada.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte char_map;					// Declara caractere mapeado.
	int len, inc = 2;				// Declara comprimento do buffer de teclado.
	SDL_PumpEvents();				// Atualiza buffer de eventos.
	const uint8_t *state = SDL_GetKeyboardState(&len);	// Adquire vetor de estados.
	for (int i = 0; i < len; i++) {				// Itera por cada termo do vec. de estados.
		if (state[i]) {					// Verifica se o estado está habilitado.
			char_map = ascii_remap(			// Remapeia o carctere de interesse.
				Vx[X].status   			// Remapeia o carctere de interesse.
			);
			if (char_map == SDL_GetKeyFromScancode(i)) {
				inc = 4;
				break;
			}

			break;
		}
	}

	PC += inc;
}

void SKUP(opcode op) {
	// Pula instrução se alguma tecla não está apertada.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte char_map;
	int len, inc = 2;
	SDL_PumpEvents();
	const uint8_t *state = SDL_GetKeyboardState(&len);
	for (int i = 0; i < len; i++) {
		if (!state[i]) {
			char_map = ascii_remap(
				Vx[X].status
			);
			if (char_map == SDL_GetKeyFromScancode(i)) {
				inc = 4;
				break;
			}
		}
	}

	PC += inc;
}

void GDELAY(opcode op) {
	// Insere delay gráfico.
	int inc = 2;					// Declara o incremento do PC. 
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	Vx[X].status = delay_timer;			// Faz a variável de delay o cconteúdo de VX.

	PC += inc;					// Incrementa o PC.
}

void KEYVR(opcode op) {
	// Verifica se alguma tecla está atrelada a algum reg.
	NOP(op);
}

void SDELAY(opcode op) {
	// Insere adquire timer global.
	int inc = 2;
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	delay_timer = Vx[X].status;

	PC += inc;
}

void SSOUND(opcode op) {
	// Adquire valor do timer de som.
	int inc = 2;
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	sound_timer = Vx[X].status;

	PC += inc;
}

void ADI(opcode op) {
	// Soma byte no registrador I.
	int inc = 2;
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	MEM[I_ptr] += Vx[X].status;

	PC += inc;
}

void FONT(opcode op) {
	// Prepara fonte.
	int inc = 2;					// Declara incremento de PC p/ 2 bytes.
	byte X  = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte *font_addr;				// Declara endereço da fonte em questão.
	printf("DEBUG: 0x%x\n", Vx[X].status);
	switch(Vx[X].status) {
		case 0x0:
			font_addr = ZERO;
			break;
		case 0x1:
			font_addr = ONE;
			break;
		case 0x2:
			font_addr = TWO;
			break;
		case 0x3:
			font_addr = THREE;
			break;
		case 0x4:
			font_addr = FOUR;
			break;
		case 0x5:
			font_addr = FIVE;
			break;
		case 0x6:
			font_addr = SIX;
			break;
		case 0x7:
			font_addr = SEVEN;
			break;
		case 0x8:
			font_addr = EIGHT;
			break;
		case 0x9:
			font_addr = NINE;
			break;
		case 0xA:
			font_addr = TEN;
			break;
		case 0xB:
			font_addr = ELEVEN;
			break;
		case 0xC:
			font_addr = TWELVE;
			break;
		case 0xD:
			font_addr = TREZE;
			break;
		case 0xE:
			font_addr = QUATORZE;
			break;
		case 0xF:
			font_addr = QUINZE;
			break;
		default:
			goto error;
			break;
	}

	for (int i = 0; i < 5; i++) {
		MEM[I_ptr + i] = font_addr[i];
	}

	PC += inc;
	return;

error:
	PC += inc;
	print_error("Fonte não declarada, valor superior a: 0xF.");
}

void XFONT(opcode op) {
	// Prepara fonte especial. --> SUPER CHIP 8
	int inc = 2;

	PC += inc;
}

void BCD(opcode op) {
	// Armazena código BCD nas posições I. 
	int inc  = 2;
	byte X   = (op & 0x0F00) >> 8;			// Recupera o índice X do registrador.
	byte num = Vx[X].status;
	MEM[I_ptr + 2] =  num % 10;
	MEM[I_ptr + 1] = ((num % 100)  - (num % 10)) /10;
	MEM[I_ptr + 0] = ((num % 1000) - (num % 100))/100;

	PC += inc;
}

void STR(opcode op) {
	// Armazena informação em endereço.
	int inc = 2;					// Declara incremento do PC como 2. 
	byte r = (op & 0x0F00) >> 8;			// Recupera nibble relativo à R.
	for(byte i = 0; i < r; i++) { 			// Itera por índices de 0 a R-1.
		MEM[I_ptr + i] = Vx[i].status;		// Atribui nos campos dos registradores I, Vx.
	}

	PC += inc;					// Incrementa o PC.
}

void LDR(opcode op) {
	// Carrega informação em registrador.
	int inc = 2;					// Declara incremento do PC como 2. 
	byte r = (op & 0x0F00) >> 8;			// Recupera nibble relativo à R.
	for(byte i = 0; i < r; i++) { 			// Itera por índices de 0 a R-1.
		Vx[i].status = MEM[I_ptr + i];		// Atribui nos campos dos registradores Vx, I.
	}

	PC += inc;					// Incrementa o PC.
}
